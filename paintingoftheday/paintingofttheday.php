<?php
/*
Plugin Name: Painting of the Day
Description: Plugin is to update painting of the day on the home page which accepts jpeg/jpg images
			 width: 280 and height 339
			 and stores it in uploads/paintingoftheday folder
			 The record is saved in the database in wpm_options with option_name as 'ma_paintingoftheday' and option_value is its respective image name.
Author: AR Solutions
Version: 1.0
Author URI: http://www.arbizsolutions.com
*/
if(!defined('ABSPATH'))
    die('You are not allowed to call this page directly.');
register_activation_hook( __FILE__, 'myplugin_activate' );
//add_action('admin_init','painting_menu');
//add_action('admin_init','paintingoftheday');
add_shortcode('disp_paintingoftheday','func_disppod');

function myplugin_activate() {
    // Activation code here...
	$upload_dir = wp_upload_dir();
	$path = $upload_dir['basedir'].'/paintingoftheday'; /* D:\xampp\htdocs\modernarts/wp-content/uploads/paintingoftheday  */
	$plugindir = plugin_dir_path( __FILE__ ); /* D:\xampp\htdocs\modernarts\wp-content\plugins\paintingofthedaytest/ */
	$imagepath=$plugindir.'default.jpg'; /* D:\xampp\htdocs\modernarts\wp-content\plugins\paintingofthedaytest/default.jpg */
	$dest=$upload_dir['basedir'].'/paintingoftheday/default.jpg';
	$options = get_option( 'ma_paintingoftheday');
	if($options==FALSE){
	   $add=add_option( 'ma_paintingoftheday', 'default.jpg', 'yes' );
	   $add=add_option( 'ma_paintingoftheday_url', 'http://modernarts.loc/shop/god-religious/natraja-1106-shiva/', 'yes' );
	   if(!file_exists($path)) {
			mkdir($path);
			if(file_exists($imagepath)){
				copy($imagepath,$dest);
			}
		}
	}

}

function func_disppod()
{
	$options = get_option( 'ma_paintingoftheday');
	$upload_dir = wp_upload_dir();
	
	$path = $upload_dir['baseurl'].'/paintingoftheday';
	$paintingpath=$path.'/'.$options;
	$option_url = get_option( 'ma_paintingoftheday_url');
     $pod = '<a title="modernarts" href="'.$option_url.'"><img class="first" title="modernarts" alt="modernarts" src="'.$paintingpath.'" /><img class="second" title="modernarts" alt="modernarts" src="'.$paintingpath.'" /></a>	';
     return $pod;
}

/*function paintingoftheday() {
	$upload_dir = wp_upload_dir();
	$path = $upload_dir['basedir'].'/paintingoftheday';
	
	$options = get_option( 'ma_paintingoftheday');
	if($options){
		//echo "Painting available.";
		//exit();
	}else{
		 $add=add_option( 'ma_paintingoftheday', 'default.jpg', 'yes' );
		//echo "123.jp has been updated!";
		//exit();
	}

	
}*/


add_action('admin_menu', 'painting_menu' );
function painting_menu() {
    add_menu_page("Painting Menu", "Painting Menu", "administrator", 'paintingmenu', "painting_menu_sample");

}

//add_action( 'admin_menu', 'painting_menu_sample' );  
function painting_menu_sample(){
	$options = get_option( 'ma_paintingoftheday');
	$upload_dir = wp_upload_dir();
	$path = $upload_dir['baseurl'].'/paintingoftheday';
	$paintingpath=$path.'/'.$options;
	$option_url = get_option( 'ma_paintingoftheday_url');
	if(isset($_FILES['upload_pod'])){
		$allowed_filetypes = array('.jpg','jpeg'); 
		$max_filesize = 524288; 
		//$width=250;
		//$height=320;
		$upload_path =$upload_dir['basedir'].'/paintingoftheday/';
		
		$error = "";
		$urlpainting = $_POST['urlpainting'];

		$filename = $_FILES['upload_pod']['name']; 
		
		$ext = substr($filename, strpos($filename,'.'), strlen($filename)-1);
		
		if(!in_array($ext,$allowed_filetypes)){
		$error = 'The file you attempted to upload is not allowed.';
		echo $error;
		}
		else{
			$image_info = getimagesize($_FILES["upload_pod"]["tmp_name"]);
			$image_width = $image_info[0];
			$image_height = $image_info[1];
			if($image_width!=280 && $image_height!=339){
				$error='Image width must be 280 and height 339';
				echo $error;
			}else{
					//$error='valid size';
					if(filesize($_FILES['upload_pod']['tmp_name']) > $max_filesize){
						$error = 'The file you attempted to upload is too large.';
						echo $error;
						
					}else{
						if(!is_writable($upload_path))
							$error = 'You cannot upload to the specified directory, please CHMOD it to 777.';
							echo $error;
							
							if(move_uploaded_file($_FILES['upload_pod']['tmp_name'],$upload_path . $filename)){
								$options = get_option( 'ma_paintingoftheday');
								
								if($options!=FALSE){
									$update=update_option('ma_paintingoftheday', $filename, 'yes');
									$update_url=update_option('ma_paintingoftheday_url', $urlpainting, 'yes');
									$error = 'Your file upload was successful!!! Please Refresh the Page to view new image!!!'; // It worked.
									echo $error;
									
								}/* if $options end */
							}/*  if move_uploaaded_file ends*/
						
					}/* else filesize ends */
				}/* else image width height ends */
		
		}/* else allowed filetype ends */
		
	}/* if isset ends */
	/*else{
		$error = 'Upload a valid Image';
		echo $error;
	}*/
	
?>
<div>
<p>Upload Image of width:280 and height:339</p>
	<br>
	<img src="<?php echo $paintingpath;?>" alt="painting of the day" title="painting of the day" width="250" height="320"><br>
	<br><br>
	
</div>						 
<form  method="post" enctype="multipart/form-data">
	<input type="text" name="urlpainting" value="<?php echo $option_url;?>"></input><br>
    <input type='file' id='upload_pod' name='upload_pod'></input>
     <?php submit_button('Upload') ?>
</form>
<?php }?>