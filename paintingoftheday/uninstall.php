<?php
 
if( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) 
	exit();
	
// Uninstall code goes here

$upload_dir = wp_upload_dir();
$options = get_option( 'ma_paintingoftheday');
$option_url = get_option( 'ma_paintingoftheday_url');
$path = $upload_dir['basedir'].'/paintingoftheday';
$paintingpath=$path.'/'.$options;
$default=$path.'/'.'default.jpg';

if ($default!=FALSE) {
	unlink($default);
	if ($options!=FALSE) {
		unlink($paintingpath);
		delete_option('ma_paintingoftheday');
		delete_option('ma_paintingoftheday_url');
		rmdir($path);
	}

}

?>